"ls": It list all files in current directory except hidden files 

"ls -l": Gives a long list of all files 

"ls -a": Will list all files including hidden files x

"cd": Is just changing the directory 

"cd ..": It takes you to the previous directory 

"cd *log": Copies all files in a directory  

"rm -r": Removes directory and their contents recursively 

"du": Stands for "Disk Usage" used to check disk usage of files and directories 

"du -h": Does same thing but puts it in a way that humans can read it

"mvn": "print out your installed version of maven"

"cp": copies a file or directory from a source to a destination

"cp * ": copies everything in a directory

"less": Starts up faster than more, allows backward and forward movement in file 

"more": View text file 1 screen at a time, does not provide as many options as less
 
"whoami": Prints username of current user when envoked 

"passwd": Ask to change password

"wc": Prints out newline, word, and byte count for files

"diff": Displays which lines is differ between two files

