\chapter{Doxygen}
\begin{quote}\it
``This chapter contains work done by: Domanik Johnson
\flushright{}
\end{quote}
\label{chapter:Doxygen}
\section{Section A of work flow}
My topic is Doxygen, Doxygen is a documentation generator, a tool for writing software reference documentation.
The documentation is written within code, and is thus relatively easy to keep up to date. Doxygen can cross reference
documentation and code, so that the reader of a document can easily refer to the actual code. 

\subsection{Examples of a picture}
Doxygen comment syntax: 

/**
 * Java Doc Style
 */
 
 /*!
 * QT style
 */
 
 Some people like to make their comment blocks more visible in the documentation. For this purpose you can use the following:

 /********************************************//**
 *  ... text
 ***********************************************/
 (note the 2 slashes to end the normal comment block and start a special comment block).

 or
 
 /////////////////////////////////////////////////
/// ... text ...
/////////////////////////////////////////////////

I can include a picture like this: 
\begin{center}
\begin{figure}
\centering
\includegraphics[width=4in]{xkcdcomic}
\caption{A cool picture from \url{www.bing.com}}
\label{fig:xkcdcomic}
\end{figure}
\end{center}
I will reference code: 
\begin{center}
\verb'int var; /*!< Detailed description after the member */ '
\end{center}

\subsection{A block of code}
A block of code example:
\begin{center}
\begin{lstlisting}[caption={A block of code}]

/*! A test class */
class Afterdoc_Test
{
  public:
    /** An enum type. 
     *  The documentation block cannot be put after the enum! 
     */
    enum EnumType
    {
      int EVal1,     /**< enum value 1 */
      int EVal2      /**< enum value 2 */
    };
    void member();   //!< a member function.
    
  protected:
    int value;       /*!< an integer value */
};

\end{lstlisting}
\end{center}